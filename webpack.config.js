/* eslint-disable */
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = (env, { mode }) => ({
	mode,
	target: 'web',
	entry: {
		app: path.resolve(__dirname, 'src', 'index.js'),
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].js',
	},
	resolve: {
		mainFiles: ['index', 'component'],
		modules: ['node_modules', 'lib', 'shared'],
		alias: {
			'store': path.resolve(__dirname, 'src', 'store'),
			'react-dom': '@hot-loader/react-dom',
			'commonStatics': path.resolve(__dirname, 'src', 'statics')
		}
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							cacheDirectory: true,
							presets: [['@babel/preset-env', {
								'targets': {
									'node': 'current'
								},
								'modules': false
							}], '@babel/preset-react'],
							plugins: [
								['@babel/plugin-proposal-class-properties', {
									'spec': true
								}],
								'@babel/plugin-transform-runtime',
								'@babel/plugin-transform-modules-commonjs',
								['@babel/plugin-proposal-decorators', {
									decoratorsBeforeExport: true,
									legacy: false
								}],
								"react-hot-loader/babel",
								'@babel/plugin-transform-react-jsx',
								'transform-react-pug',
								'lodash'
							]
						}
					}
				]
			},
			{
				test: /\.cson$/,
				use: {
					loader: 'cson-loader'
				}
			},
			{
				test: /\.pug$/,
				use: {
					loader: 'pug-loader'
				}
			},
			{
				test: /\.raw\.svg$/,
				use: {
					loader: 'raw-loader'
				}
			},
			{
				test: /\.preload\.(png|jpg|gif|ttf|woff|woff2|eot|svg|otf)$/,
				use: {
					loader: 'url-loader'
				}
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					}
				]
			},
			{
				test: filePath => {
					return (/\.(webp|ttf|woff|woff2|eot|svg|otf|png|jpg|gif)$/).test(filePath) &&
						!(/\.preload\.(webp|ttf|woff|woff2|eot|svg|otf|png|jpg|gif)$/).test(filePath) &&
						!(/\.raw\.svg$/).test(filePath)
				},
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[hash].[ext]',
							outputPath: 'statics/'
						}
					}
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new webpack.ProvidePlugin({
			React: 'react'
		}),
		new HtmlWebpackPlugin({
			filename: path.resolve(__dirname, 'dist', 'index.html'),
			template: './index.pug'
		})
	],
	...(mode === 'development' ? {
			devServer: {
				port: 9000,
				hot: true,
				inline: true
			}
		} : {})
})