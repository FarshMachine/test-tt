import {
	applyMiddleware,
	createStore
} from 'redux'

import { combineReducers } from 'redux-immutable'
import { composeWithDevTools } from 'redux-devtools-extension'
import ownersReducer from './branches/tasks'
import performersReducer from './branches/performers'
import thunk from 'redux-thunk'

const reducer = combineReducers({
	performers: performersReducer,
	tasks: ownersReducer
})

export default createStore(
	reducer,
	composeWithDevTools(applyMiddleware(thunk))
)