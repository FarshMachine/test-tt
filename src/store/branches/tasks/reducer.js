import {
	addComment,
	setPerformer
} from './actions'

import { fromJS, merge } from 'immutable'
import { handleActions } from 'redux-actions'
import initialState from './initialState.cson'

export default handleActions({
	[addComment]: (state, { payload: { id, comment } }) => {
		return state.updateIn([id, 'comments'], (comments) => comments.merge(comment))
	},
	[setPerformer]: (state, { payload: { id, performer } }) => {
		return state.setIn([id, 'performer'], performer)
	}
}, fromJS(initialState))
