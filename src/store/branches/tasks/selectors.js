/*eslint-disable*/

export const getTasks = () => state => state.get('tasks')
export const getTaskId = (id) => state => state.getIn(['tasks', id])
export const getTaskTitle = (id) => state => state.getIn(['tasks', id, 'title'])
export const getTaskPerformer = (id) => state => state.getIn(['tasks', id, 'performer'])
export const getTaskAddress = (id) => state => state.getIn(['tasks', id, 'address'])
export const getTaskReason = (id) => state => state.getIn(['tasks', id, 'reason'])
export const getTaskFlatComment = (id) => state => state.getIn(['tasks', id, 'flatComment'])

export const getTaskOwners = (id) => state => state.getIn(['tasks', id, 'owners'])
export const getTaskOwnerName = (id, index) => state => state.getIn(['tasks', id, 'owners', index, 'name'])
export const getTaskOwnerStatus = (id, index) => state => state.getIn(['tasks', id, 'owners', index, 'status'])
export const getTaskOwnerLawState = (id, index) => state => state.getIn(['tasks', id, 'owners', index, 'lawState'])
export const getTaskLsNumber = (id, index) => state => state.getIn(['tasks', id, 'owners', index, 'lsNumber'])
export const getTaskTelNumber = (id, index) => state => state.getIn(['tasks', id, 'owners', index, 'telNumber'])

export const getTaskComments = (id) => state => state.getIn(['tasks', id, 'comments'])
export const getTaskComment = (id, index) => state => state.getIn(['tasks', id, 'comments', index])

export const getTaskTimestampStart = (id) => state => state.getIn(['tasks', id, 'timestamp', 'start'])
export const getTaskTimestampEnd = (id) => state => state.getIn(['tasks', id, 'timestamp', 'end'])

export const getTaskTags = (id) => state => state.getIn(['tasks', id, 'tags'])
export const getTaskTagText = (id, index) => state => state.getIn(['tasks', id, 'tags', index, 'text'])
export const getTaskTagColor = (id, index) => state => state.getIn(['tasks', id, 'tags', index, 'color'])
