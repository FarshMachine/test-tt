import { createActions } from 'redux-actions'

export const {
	addComment,
	setPerformer,
} = createActions('ADD_COMMENT', 'SET_PERFORMER')
