import { fromJS } from 'immutable'
import { handleActions } from 'redux-actions'

const initialState = ['Василий', 'Иван', 'Бугульмек']

export default handleActions({ }, fromJS(initialState))
