import './style.css'
import 'antd/dist/antd.css'

import { HashRouter, Redirect } from 'react-router-dom'

import Content from './components/Content'
import { Layout } from 'antd'
import { Provider } from 'react-redux'
import Sider from './components/Sider'
import { ThemeProvider } from 'styled-components'
import { hot } from 'react-hot-loader/root'
import { render } from 'react-dom'
import store from 'store'
import { theme } from 'config'

let App = () => pug`
	Provider(store=store)
		ThemeProvider(theme=theme)
			HashRouter
				Redirect(from="/" to="tasks")
				Layout(theme='light')
					Sider
					Content

`

if (module.hot) {
	App = hot(App)
}

render(pug`App`, document.getElementById('root'))