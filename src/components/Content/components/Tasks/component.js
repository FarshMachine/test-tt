import Header from './components/Header'
import Page from 'Page'
import Task from './components/Task'

const TasksComponent = () => {
	return pug`
		Page
			Header
			Task(id='0')
	`
}

export default TasksComponent