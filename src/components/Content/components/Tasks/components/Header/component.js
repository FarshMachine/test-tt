import BackButton from 'BackButton'
import { Header } from './style'

const HeaderComponent = () => {
	return pug`
		Header
			BackButton
	`
}

export default HeaderComponent