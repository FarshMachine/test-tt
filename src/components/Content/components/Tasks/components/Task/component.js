import { Col, Row } from 'antd'

import Comment from './components/Comment'
import Information from './components/Information'
import Performers from './components/Performers'
import ProgressLine from './components/ProgressLine'
import Stages from './components/Stages'
import Title from './components/Title'

const TaskComponent = ({ id: taskId }) => {
	return pug`
		Title(taskId=taskId)
		ProgressLine(taskId=taskId)
		Performers(taskId=taskId)
		Row(gutter=[16, 0])
			Col(span=16)
				Comment(taskId=taskId)
				Information(taskId=taskId)
			Col(span=5)
				Stages

	`
}

export default TaskComponent