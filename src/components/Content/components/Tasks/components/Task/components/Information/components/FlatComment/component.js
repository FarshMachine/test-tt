import TaskInformationItem from 'TaskInformationItem'
import { getTaskFlatComment } from 'store/branches/tasks'
import { useSelector } from 'react-redux'

const FlatCommentComponent = ({ taskId }) => {
	const flatComment = useSelector(getTaskFlatComment(taskId))

	return pug`
		TaskInformationItem(fieldName='Комментарий к квартире')
			=flatComment
	`
}

export default FlatCommentComponent