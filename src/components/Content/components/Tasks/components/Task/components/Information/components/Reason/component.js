import TaskInformationItem from 'TaskInformationItem'
import { getTaskReason } from 'store/branches/tasks'
import { useSelector } from 'react-redux'

const ReasonComponent = ({ taskId }) => {
	const taskReason = useSelector(getTaskReason(taskId))

	return pug`
		TaskInformationItem(fieldName='Причина задачи')
			=taskReason
	`
}

export default ReasonComponent