import { AddCommentButton, Comment, Icon, StyledInput, Title } from './style'
import { Col, Row } from 'antd'

import { addComment } from 'store/branches/tasks'
import { useDispatch } from 'react-redux'
import { useState } from 'react'

const CommentComponent = ({ taskId }) => {
	const [comment, setComment] = useState('')
	const dispatch = useDispatch()

	const onChangeHandler = ({ target }) => {
		setComment(target.value)
	}

	const addCommentHandler = () => {
		if (comment.length) {
			dispatch(addComment({
				comment,
				id: taskId
			}))
			setComment('')
		}
	}

	return pug`
		Comment
			Col(span=24)
				Title Комментарии к задаче
				Row(gutter=[16, 0])
					Col
						Icon
					Col(span=22)
						StyledInput(onChange=onChangeHandler value=comment)
						Row
							AddCommentButton(
								type='primary'
								disabled=comment.length === 0
								onClick=addCommentHandler
							) Добавить комментарий
	`
}

export default CommentComponent