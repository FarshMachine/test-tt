import { Row } from 'antd'
import styled from 'styled-components'

export const Title = styled(Row)`
	font-size: 32px;
	font-weight: 300;
	margin-top: 32px;
`