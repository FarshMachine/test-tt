import { Row, Steps } from 'antd'
import styled from 'styled-components'

const { Step } = Steps

export const Title = styled(Row)`
	font-size: 24px;
	margin-bottom: 18px;
`

export const StyledStep = styled(Step)`
	&.ant-steps-item-wait {
		.ant-steps-item-icon {
			border: none;
			background-color: #F3F5F6;
		}

		.ant-steps-item-description {
			opacity: .6;
		}

		span {
			opacity: .6;
			color: ${({ theme }) => theme.colors.main} !important;
		}

		
	}

	&.ant-steps-item-active {
		.ant-steps-item-tail::after {
			background-color: #1890ff;
		}
	}

	.ant-steps-item-icon {
		display: flex;
		align-items: center;
		justify-content: center;
		font-size: 12px;
	}

	.ant-steps-item-description {
		color: ${({ theme }) => theme.colors.main} !important;
		font-size: 13px;
	}

	.ant-steps-icon {
		display: flex;
		align-items: center;
		justify-content: center;
	}
`