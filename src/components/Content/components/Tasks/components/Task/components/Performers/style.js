import { Button, Col, Row, Select } from 'antd'
import styled from 'styled-components'

export const Performer = styled(Col)`
	padding: 10px;
	box-shadow: 0px 4px 4px rgba(78, 93, 146, 0.16);
	margin-bottom: 28px;
`

export const Title = styled(Row)`
	opacity: .6;
`

export const StyledSelect = styled(Select)`
	width: 100%;

	div {
		border-radius: 4px !important;
	}
`

export const StyledButton = styled(Button)`
	width: 100%;
	border-radius: 4px;
`