import Address from './components/Address'
import FlatComment from './components/FlatComment'
import Number from './components/Number'
import Owners from './components/Owners'
import Reason from './components/Reason'
import StartDate from './components/StartDate'
import Tags from './components/Tags'
import { Title } from './style'

const InformationComponent = ({ taskId }) => {
	return pug`
		Title Подробная Информация
		Reason(taskId=taskId)
		Number(taskId=taskId)
		StartDate(taskId=taskId)
		Address(taskId=taskId)
		FlatComment(taskId=taskId)
		Tags(taskId=taskId)
		Owners(taskId=taskId)
	`
}

export default InformationComponent