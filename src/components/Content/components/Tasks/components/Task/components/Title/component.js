import { Title } from './style.js'
import { getTaskTitle } from 'store/branches/tasks'
import { useSelector } from 'react-redux'

const TitleComponent = ({ taskId }) => {
	const title = useSelector(getTaskTitle(taskId))

	return pug`
		Title
			=title
	`
}

export default TitleComponent