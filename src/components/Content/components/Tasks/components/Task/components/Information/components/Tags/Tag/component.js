import { getTaskTagColor, getTaskTagText } from 'store/branches/tasks'

import { Tag } from 'antd'
import { useSelector } from 'react-redux'

const TagComponent = ({ taskId, tagIndex }) => {
	const tagText = useSelector(getTaskTagText(taskId, tagIndex))
	const tagColor = useSelector(getTaskTagColor(taskId, tagIndex))

	return pug`
		Tag(color=tagColor)
			=tagText
	`
}

export default TagComponent