import { Col, Row, Select } from 'antd'
import { Performer, StyledButton, StyledSelect, Title } from './style'
import { useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { getPerformers } from 'store/branches/performers'
import { setPerformer } from 'store/branches/tasks'

const { Option } = Select

const renderPerformers = (performers) => {
	return performers.map((performer) => {
		return pug`
			Option(key=performer)
				=performer
		`
	})
}

export const PerformersComponent = ({ taskId }) => {
	const [chosenPerformer, setChosenPerformer] = useState(null)
	const performers = useSelector(getPerformers())
	const dispatch = useDispatch()

	const selectHandler = useCallback((performer) => {
		setChosenPerformer(performer)
	}, [chosenPerformer])

	const buttonHandler = useCallback(() => {
		if (chosenPerformer) {
			dispatch(setPerformer({
				id: taskId,
				performer: chosenPerformer
			}))
		}
	}, [chosenPerformer])

	return pug`
		Performer
			Title Исполнитель
			Row(gutter=16 align='middle')
				Col(span=18)
					StyledSelect(
						placeholder='Выберете исполнителя' 
						size='large'
						onChange=selectHandler
					)
						=${renderPerformers(performers)}
				Col(span=6)
					StyledButton(
						type="primary"
						size='large'
						onClick=buttonHandler
					) Завершить этап
	`
}

export default PerformersComponent