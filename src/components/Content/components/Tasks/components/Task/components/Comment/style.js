import { Button, Input, Row } from 'antd'

import styled from 'styled-components'
import userIco from 'commonStatics/user-ico.svg'

export const Comment = styled(Row)`
	margin-bottom: 36px;
`

export const StyledInput = styled(Input)`
	border-radius: 4px;
	margin-bottom: 16px;
`

export const Icon = styled.div`
	width: 32px;
	height: 32px;
	border-radius: 50%;
	background: url(${userIco}) center no-repeat;
	background-color: #F3F5F6;
`

export const AddCommentButton = styled(Button)`
	border-radius: 4px;
`

export const Title = styled(Row)`
	font-size: 24px;
	margin-bottom: 18px;
`
