import { Col, Steps } from 'antd'
import { StyledStep, Title } from './style'

import { FinishIco } from './FinishIco'
import Icon from '@ant-design/icons'

const StagesComponent = () => {
	return pug`
		Col(span=24)
			Title Этапы выполнения
			Steps(direction='vertical')
				StyledStep(status='process' description='Назначить сотрудника для проведения внеплановой проверки')
				StyledStep(description='Добавить Акт проверки прибора / акт об отказу в допуске')
				StyledStep(description='Завершение задачи' icon=${pug`Icon(component=FinishIco)`})
	`
}

export default StagesComponent