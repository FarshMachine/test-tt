import moment from 'moment'

const PERCENTS_KF = 100

export const timeDiff = (timestampStart, timestampEnd) => {
	const startDate = moment(timestampStart)
	const endDate = moment(timestampEnd)
	const daysPassed = moment(new Date()).diff(startDate, 'millisecond')
	const totalTime = endDate.diff(startDate, 'millisecond')
	const parsedTotalTime = moment.utc(totalTime).format(`D[д] H[ч] (до${endDate.format('DD.MM.YY')})`)

	return [
		Math.round(daysPassed / totalTime * PERCENTS_KF),
		parsedTotalTime
	]
}