import TaskInformationItem from 'TaskInformationItem'

const NumberComponent = ({ taskId }) => {

	return pug`
		TaskInformationItem(fieldName='Номер задачи')
			=taskId
	`
}

export default NumberComponent