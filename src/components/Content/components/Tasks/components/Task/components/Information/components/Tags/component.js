import Tag from './Tag'
import TaskInformationItem from 'TaskInformationItem'
import { getTaskTags } from 'store/branches/tasks'
import { useSelector } from 'react-redux'

const renderTags = (taskId, tags) => {
	return tags.map((tag, index) => {
		return pug`
			Tag(key=tag.get('text') taskId=taskId tagIndex=index)
		`
	})
}

const TagsComponent = ({ taskId }) => {
	const tags = useSelector(getTaskTags(taskId))

	return pug`
		TaskInformationItem(fieldName='Теги')
			=renderTags(taskId, tags)
	`
}

export default TagsComponent