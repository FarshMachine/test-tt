import { Row } from 'antd'
import styled from 'styled-components'

export const Title = styled(Row)`
	font-size: 24px;
	margin-bottom: 18px;
`