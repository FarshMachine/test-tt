import {
	ClockIcon, ProgressLine,
	Reason, StageTime,
	TaskTime, TaskTimeText
} from './style'
import { Col, Progress } from 'antd'
import {
	getTaskReason,
	getTaskTimestampEnd,
	getTaskTimestampStart
} from 'store/branches/tasks'

import { timeDiff } from './kit.js'
import { useMemo } from 'react'
import { useSelector } from 'react-redux'

const TimelineComponent = ({ taskId }) => {
	const timestampStart = useSelector(getTaskTimestampStart(taskId))
	const timestampEnd = useSelector(getTaskTimestampEnd(taskId))
	const reason = useSelector(getTaskReason(taskId))

	const [
		percent,
		parsedTotalTime
	] = useMemo(() => {
		return timeDiff(timestampStart, timestampEnd)
	}, [timestampStart, timestampEnd])

	return pug`
		ProgressLine(gutter=[10, 0])
			Col(span=20)
				Reason
					=reason
				Progress(
					percent=percent
					strokeColor='#17B45A'
					showInfo=false
				)
				StageTime(align='middle')
					ClockIcon
					=${`Время на этап: ${parsedTotalTime}`}
			Col(span=4)
				TaskTimeText Время на задачу
				TaskTime
					=parsedTotalTime
	`
}

export default TimelineComponent