import { Row } from 'antd'
import clockIco from './statics/clock-ico.svg'
import styled from 'styled-components'

export const ProgressLine = styled(Row)`
	margin-top: 16px;
	margin-bottom: 8px;
`

export const Reason = styled(Row)`
	margin-bottom: 11px;
	opacity: .8;
	font-size: 14px;
`

export const StageTime = styled(Row)`
	margin-top: 8px;
	opacity: .6;
	font-size: 14px;
`

export const ClockIcon = styled.div`
	width: 16px;
	height: 16px;
	margin-right: 10px;
	background: url(${clockIco}) center no-repeat;
`

export const TaskTime = styled(StageTime)`
	margin: 0;
	opacity: .8;
`

export const TaskTimeText = styled(Row)`
	margin-bottom: 4px;
	opacity: .6;
	font-size: 14px;
`