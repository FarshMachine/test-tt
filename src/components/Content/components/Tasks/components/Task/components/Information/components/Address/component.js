import TaskInformationItem from 'TaskInformationItem'
import { getTaskAddress } from 'store/branches/tasks'
import { useSelector } from 'react-redux'

const AddressComponent = ({ taskId }) => {
	const taskAddress = useSelector(getTaskAddress(taskId))

	return pug`
		TaskInformationItem(fieldName='Причина задачи' fw=500 opacity=1)
			=taskAddress
	`
}

export default AddressComponent