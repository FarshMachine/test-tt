import {
	getTaskLsNumber,
	getTaskOwnerLawState,
	getTaskOwnerName,
	getTaskOwnerStatus,
	getTaskTelNumber
} from 'store/branches/tasks'

import TaskInformationItem from 'TaskInformationItem'
import { useSelector } from 'react-redux'

const OwnerComponent = ({ taskId, ownerIndex }) => {
	const lsNumber = useSelector(getTaskLsNumber(taskId, ownerIndex))
	const lawState = useSelector(getTaskOwnerLawState(taskId, ownerIndex))
	const name = useSelector(getTaskOwnerName(taskId, ownerIndex))
	const status = useSelector(getTaskOwnerStatus(taskId, ownerIndex))
	const telNumber = useSelector(getTaskTelNumber(taskId, ownerIndex))

	return pug`
		TaskInformationItem(fieldName=${`Собственник${ownerIndex + 1}`} fw=500 opacity=1)
			=name
		TaskInformationItem(fieldName=${`Статус собственника${ownerIndex + 1}`})
			=status
		TaskInformationItem(fieldName='Юридическое состояние')
			=lawState
		TaskInformationItem(fieldName=${`Номер ЛС собственника${ownerIndex + 1}`})
			=lsNumber
		TaskInformationItem(fieldName='Контактный номер телефона')
			=telNumber
	`
}

export default OwnerComponent