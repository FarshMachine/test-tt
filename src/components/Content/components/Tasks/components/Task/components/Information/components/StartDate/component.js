import TaskInformationItem from 'TaskInformationItem'
import { getTaskTimestampStart } from 'store/branches/tasks'
import moment from 'moment'
import { useSelector } from 'react-redux'

const StartDateComponent = ({ taskId }) => {
	const taskTimestampStart = useSelector(getTaskTimestampStart(taskId))
	const formattedDate = moment.utc(taskTimestampStart).format(`DD:MM:YY hh:mm`)

	return pug`
		TaskInformationItem(fieldName='Дата создания')
			=formattedDate
	`
}

export default StartDateComponent