import Owner from './Owner'
import { getTaskOwners } from 'store/branches/tasks'
import { useSelector } from 'react-redux'

const renderOwners = (taskId, owners) => {
	return owners.map((owner, index) => {
		return pug`
			Owner(key=owner.get('name') taskId=taskId ownerIndex=index)
		`
	})
}

const TagsComponent = ({ taskId }) => {
	const owners = useSelector(getTaskOwners(taskId))

	return pug`
		=renderOwners(taskId, owners)
	`
}

export default TagsComponent