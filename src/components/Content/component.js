import AnimatedSwitch from 'AnimatedSwitch'
import { Layout } from 'antd'
import Objects from './components/Objects'
import Owners from './components/Owners'
import { Route } from 'react-router-dom'
import Settings from './components/Settings'
import Tasks from './components/Tasks'

const ContentComponent = () => pug`
	Layout(style={ position: 'relative'})
		AnimatedSwitch
			Route(path='/tasks' component=Tasks)
			Route(path='/objects' component=Objects)
			Route(path='/owners' component=Owners)
			Route(path='/settings' component=Settings)
`

export default ContentComponent