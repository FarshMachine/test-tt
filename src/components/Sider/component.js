import AuthInfo from './components/AuthInfo'
import Header from './components/Header'
import Menu from './components/Menu'
import { StyledSider } from './style.js'

const MenuComponent = () => {
	return pug`
		StyledSider
			Header
			AuthInfo
			Menu
	`
}

export default MenuComponent