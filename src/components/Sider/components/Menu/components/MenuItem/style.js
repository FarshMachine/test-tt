import { DURATION } from 'config'
import { Row } from 'antd'
import styled from 'styled-components'

export const MenuItem = styled(Row)`
	position: relative;
	margin-bottom: 16px;
	padding-left: 16px;
	cursor: pointer;

	:last-child {
		margin-bottom: 0;
	}

	div {
		color: ${({ selected, theme: { colors } }) => selected ? colors.selected : colors.main};
	}
`

export const Pointer = styled.div`
	position: absolute;
	left: 0;
	top: 0;
	height: 100%;
	width: 2px;
	border-radius: 0 10px 10px 0;
	background-color: ${({ theme: { colors } }) => colors.selected};
`

export const Text = styled.div`
	margin-left: 19px;
	font-size: 14px;
	font-weight: 500;
	
	${MenuItem}:hover & {
		color:${({ theme: { colors } }) => colors.selected};
	}

	transition: color ${DURATION.FAST}ms ease;
`

export const Ico = styled.div`
	width: 16px;
	height: 16px;
	background-color: ${({ selected, theme: { colors } }) => selected ? colors.selected : colors.main};
	mask-image: url(${({ url }) => url});
	mask-position: center;
	mask-repeat: no-repeat;
	
	${MenuItem}:hover & {
		background-color: ${({ theme: { colors } }) => colors.selected};
	}

	transition: background-color ${DURATION.FAST}ms ease;
`