import { Ico, MenuItem, Pointer, Text } from './style'
import { useHistory, useLocation } from 'react-router-dom'

import objectsIco from './statics/objects-ico.svg'
import ownersIco from './statics/owners-ico.svg'
import settingIco from './statics/settings-ico.svg'
import tasksIco from './statics/tasks-ico.svg'
import { useCallback } from 'react'

const icons = {
	'/objects': objectsIco,
	'/owners': ownersIco,
	'/settings': settingIco,
	'/tasks': tasksIco,
}

const MenuItemComponent = ({ name, children }) => {
	const { pathname } = useLocation()
	const history = useHistory()

	const selected = name === pathname

	const setPage = useCallback(() => {
		history.push(name)
	}, [name])

	return pug`
		MenuItem(
			align='middle'
			selected=selected
			onClick=setPage
		)
			if selected
				Pointer
			Ico(url=icons[name] selected=selected)
			Text
				=children
	`
}

export default MenuItemComponent