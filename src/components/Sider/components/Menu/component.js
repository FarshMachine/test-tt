import { Menu } from './style'
import MenuItem from './components/MenuItem'

const MenuComponent = () => {
	return pug`
		Menu
			MenuItem(name='/tasks') Задачи
			MenuItem(name='/objects') Объекты
			MenuItem(name='/owners') Собственники
			MenuItem(name='/settings') Настройки
	`
}

export default MenuComponent