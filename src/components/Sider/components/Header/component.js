import { Header, Logo, TitleMedium, TitleRegular } from './style'

const HeaderComponent = () => {
	return pug`
		Header(align='middle')
			Logo
			span
				TitleMedium TT&nbsp
				TitleRegular Management
	`
}

export default HeaderComponent