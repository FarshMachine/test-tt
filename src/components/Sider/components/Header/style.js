import { Row } from 'antd'
import logo from './statics/logo.png'
import styled from 'styled-components'

export const Header = styled(Row)`
	margin-top: 8px;
	padding: 8px;
	box-sizing: border-box;
	font-size: 16px;
`

export const Logo = styled.div`
	width: 40px;
	height: 32px;
	margin-right: 8px;
	background: url(${logo}) center no-repeat;
`

export const TitleMedium = styled.span`
	font-weight: 500;
`

export const TitleRegular = styled.span`
	font-weight: 400;
`