import { Row } from 'antd'
import styled from 'styled-components'
import userIco from 'commonStatics/user-ico.svg'

export const AuthInfo = styled(Row)`
	margin-top: 16px;
`

export const UserInfo = styled(Row)`
	padding-top: 8px;
	padding-bottom: 8px;
`

export const Icon = styled.div`
	width: 16px;
	height: 16px;
	margin-right: 5px;
	background: url(${userIco}) center no-repeat;
`

export const Mail = styled.div`
	font-size: 14px;
`

export const Navigation = styled(Row)`
	opacity: .6;
`

export const LogoutButton = styled(Row)`
	padding-top: 8px;
	padding-bottom: 8px;
	font-size: 12px;
	font-weight: 500;
`