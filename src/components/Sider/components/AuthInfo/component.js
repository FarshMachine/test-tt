import {
	AuthInfo, Icon,
	LogoutButton, Mail,
	Navigation, UserInfo
} from './style'

const AuthInfoComponent = () => {
	return pug`
		AuthInfo(justify='center')
			UserInfo(align='middle' justify='center')
				Icon
				Mail Username@yandex.ru
				Navigation УК «Лесные озёра»
			LogoutButton Выход из системы
	`
}

export default AuthInfoComponent

