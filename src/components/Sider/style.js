import { Layout, Row } from 'antd'
import styled from 'styled-components'

const { Sider } = Layout

export const StyledSider = styled(Sider)`
	min-height: 100vh;
	background-color: transparent;
`

export const StyledMenu = styled(Row)`
	background-color: transparent;
`