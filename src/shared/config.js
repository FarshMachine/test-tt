export const theme = {
	colors: {
		main: '#272F5A',
		selected: '#189EE9'
	}
}

export const DURATION = {
	FAST: 200,
	FASTER: 100,
	NORMAL: 300,
	SLOW: 400,
	SLOWER: 500
}

export const DELAY = DURATION