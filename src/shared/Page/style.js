import { DURATION } from 'config'
import { Layout } from 'antd'
import styled from 'styled-components'

const { Content } = Layout

export default styled(Content)`
	background-color: white;
	position: absolute;
	min-width: 100%;
	min-height: 100%;
	top: 0;
	left: 0;

	&.enter {
		touch-action: none;
		pointer-events: none;
		opacity: 0;
	}
	&.enter-active {
		opacity: 1;
		transition: opacity ${DURATION.NORMAL}ms;
	}
	&.exit  {
		pointer-events: none;
		touch-action: none;
		opacity: 1;
	}
	&.exit-active {
	  opacity: 0;
	  transition: opacity ${DURATION.NORMAL}ms ease;
	}
`