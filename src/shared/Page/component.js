import { Col } from 'antd'
import Page from './style.js'

const PageComponent = ({ children }) => {
	return pug`
		Page
			Col(push=1 span=22)
				=children
	`
}

export default PageComponent