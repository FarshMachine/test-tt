import { DURATION } from 'config'
import { Row } from 'antd'
import backIcon from './statics/back-icon.svg'
import styled from 'styled-components'

export const BackButton = styled(Row)`
	&:hover {
		div {
			color: ${({ theme: { colors } }) => colors.selected};
		}
	}
`

export const Icon = styled.div`
	display: inline-block;
	width: 16px;
	height: 16px;
	margin-right: 10px;
	mask: url(${backIcon});
	background-color: ${({ theme: { colors } }) => colors.main};

	${BackButton}:hover & {
		background-color: ${({ theme: { colors } }) => colors.selected};
	}
	
	transition: background-color ${DURATION.FAST}ms ease;
`

export const Text = styled.div`
	font-weight: 500;
	transition: color ${DURATION.FAST}ms ease;
`