import { BackButton, Icon, Text } from './style'
import { Col } from 'antd'

export const BackButtonComponent = () => {
	return pug`
		Col(span=3)
			BackButton(align='middle')
				Icon
				Text Назад
	`
}

export default BackButtonComponent