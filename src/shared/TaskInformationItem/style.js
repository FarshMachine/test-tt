import { Col, Row } from 'antd'
import styled from 'styled-components'

export const TaskInformationItem = styled(Row)`
	border-bottom: 1px solid #DCDEE4;
	font-size: 14px;
	padding: 15px;
`

export const NameSide = styled(Col)`
	opacity: .6
`

export const ContentSide = styled(Col)`
	font-weight: ${({ fw }) => fw ? fw : '400'};
	opacity: ${({ opacity }) => opacity ? opacity : '.8'};
`