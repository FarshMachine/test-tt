import { ContentSide, NameSide, TaskInformationItem } from './style'

const TaskInformationItemComponent = ({ fieldName, children, fw, opacity }) => {
	return pug`
		TaskInformationItem(gutter=[16, 0])
			NameSide(span=11)
				=fieldName
			ContentSide(span=11 fw=fw opacity=opacity)
				=children
	`
}

export default TaskInformationItemComponent